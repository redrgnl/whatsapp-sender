const command = require('../modules.js');
const fs = require('fs')
const express = require('express');
const app = express();
const port = 1771; // parameters

app.use(express.json({ limit: '50mb' }));

const { Client, LocalAuth, MessageMedia } = require('whatsapp-web.js');
const qrcode = require('qrcode-terminal');
const whatsapp = new Client({
    authStrategy: new LocalAuth()
});

whatsapp.on('qr', qr => {
    qrcode.generate(qr, {
        small: true
    });
});

app.post('/execute', (request, response) => {
    var phone = request.body.phone;
    phone = phone + '@c.us';

    var template = fs.readFileSync('../../json/template.json');
    template = JSON.parse(template);

    setTimeout(function() {
        if (template.type == 'embed') {
            whatsapp.sendMessage(phone, MessageMedia.fromFilePath('../../asset/embed.png'), {caption: template.message})
        } else {
            whatsapp.sendMessage(phone, template.message)
        }

        command.report(phone).then(result => console.log(result));
    }, 3000);

    response.send({
        status: 'success'
    })
});

whatsapp.on('ready', () => {
    console.log('[#] Whatsapp Gateway 1 is Ready')
});

whatsapp.initialize();

app.listen(port, () => {
    console.log(`[#] Whatsap Express 1 ${port} is Ready`);
})