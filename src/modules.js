const fs = require('fs').promises
const fsSync = require('fs')
const nodeXlsx = require('node-xlsx');
const fetch = require('node-fetch');
const maxGateway = 2;

const mysql = require('mysql');
const util = require('util');
const connection = mysql.createConnection({
    host: 'localhost',
    user: 'root',
    password: '',
    database: 'whatsapp'
});
const query = util.promisify(connection.query).bind(connection);
const sleep = require('util').promisify(setTimeout);

const report = (data) => new Promise (async (resolve, reject) => {
    try {
        await query("INSERT INTO REPORT VALUES (DEFAULT, '" + data + "', 'SUCCESS', DEFAULT)");
        resolve({
            status: 'success',
            message: data
        })
    } catch (error) {
        resolve({
            status: 'error',
            message: error
        })
    }
});

const generateGateway = (data) => new Promise (async (resolve, reject) => {
    for (var i = 1; i <= data; i++) {
        try {
            await fs.rmdir('./src/gateway'+i, { recursive: true });
            console.log('[#] Delete Gateway '+ i +' Directory Success');

        } catch (err) { 
            console.log('[!] Delete Gateway '+ i +' Directory Error!!');
        }

        // create directory
        await fs.mkdir('./src/gateway' + i);
        
        var baseWhatsapp = await fs.readFile('./src/base-whatsapp.txt');
        baseWhatsapp = baseWhatsapp.toString().replace('!!port!!', 1770 + i).replaceAll('!!number!!', i)
        await fs.writeFile('./src/gateway' + i + '/whatsapp.js', baseWhatsapp, 'utf8');
    }

    resolve({
        status: "success",
        message: ""
    })
});

const setupFiles = () => new Promise (async (resolve, reject) => {
    try {
        await fs.readFile('./json/target.json');
    } catch (err) {
        await fs.writeFile('./json/target.json', JSON.stringify([{}], null, 2), 'utf8');
    }

    try {
        await fs.readFile('./json/template.json');
    } catch (err) {
        await fs.writeFile('./json/template.json', JSON.stringify({ type: "", message: "" }, null, 2), 'utf8');
    }

    resolve({
        status: "success",
        message: "[#] Setting Up Files Completed"
    })
});

const convertNumber = (data) => new Promise (async (resolve, reject) => {
    var splits = data.toString().split('');
    data = data.toString();

    if (splits[0] == 0) {
        data = '62' + data.substring(1);
    }

    resolve(data)
});

const processExcel = () => new Promise (async (resolve, reject) => {
    var files = await fs.readdir('../asset/');
    var xlsx = [];

    for (var i = 0; i < files.length; i++) {
        var ext = files[i].split('.');
        ext = ext.pop();

        var fileExt = ['xlsx', 'xls'];

        if (fileExt.includes(ext)) {
            xlsx.push(files[i]);
        }
    }

    var numbers = [];
    for (var i = 0; i < xlsx.length; i++) {
        var arrays = nodeXlsx.parse(`../asset/` + xlsx[i]);
        for (var j = 0; j < arrays[0].data.length; j++) {
            if (arrays[0].data[j][0] != undefined) {
                var converted = await convertNumber(arrays[0].data[j][0]);
                numbers.push(converted);
            }
        }
    }

    try {
        await fs.unlink('../json/target.json');
        await fs.writeFile('../json/target.json', JSON.stringify(numbers, null, 2), 'utf8');
    } catch (err) {
        await fs.writeFile('../json/target.json', JSON.stringify(numbers, null, 2), 'utf8');
    }
    
    resolve({
        status: 'success',
        message: ''
    })
});

const executeMessage = () => new Promise (async (resolve, reject) => {
    var numbers = await fs.readFile('./json/target.json');
    numbers = JSON.parse(numbers);

    var gateway = 1;
    for (var i = 0; i < numbers.length; i++) {
        await sleep(3000);

        try {
            var url = 'http://localhost:177' + gateway + '/execute';
            var body = { phone: numbers[i] };
            var request = await fetch(url, {
                method: 'POST',
                body: JSON.stringify(body),
                headers: {
                    'Content-Type': 'application/json'
                }
            });
            await request.json();

        } catch (error) {
            console.log('[!] Sending '+ numbers[i] +' Error, Gateway ' + gateway + ' is Offline');
            i--;
        }
        
        gateway++;
        (gateway > maxGateway) ? gateway = 1 : gateway = gateway;
    }

    resolve({
        status: "success",
        message: ""
    })
});

module.exports = {
    report,
    setupFiles,
    processExcel,
    executeMessage,
    generateGateway
}

// (async () => {
//     var hehe = await executeMessage();
//     console.log(hehe)
// })()