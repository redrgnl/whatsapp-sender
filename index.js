const command = require('./src/modules.js');

const express = require('express');
const app = express();
const port = 1770;

app.use(express.json({ limit: '50mb' }));

// generate template and target json
app.get('/setup-files', async (request, response) => {
    await command.setupFiles();

    response.send({
        status: "success",
        message: "[#] Setting Up Files Completed"
    })
});

// process excel files number into target json
app.get('/process-excel', async (request, response) => {
    var result = await command.processExcel();
    response.send(result);
});

// generate whatsapp gateway : on going
app.get('/generate-gateway/:number', async (request, response) => {
    var result = await command.generateGateway(request.params.number)
    response.send(result);
});

// sending whatsapp message
app.get('/execute-message', async (request, response) => {
    var result = await command.executeMessage();
    response.send(result);
});



app.listen(port, () => {
    console.log(`[#] Main Blaster ${port} is Ready`);
})